package az.cinemaplus.cinemaplus.controller;

import az.cinemaplus.cinemaplus.entity.Film;
import az.cinemaplus.cinemaplus.service.FilmService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/films")
public class FilmController {
    private final FilmService filmService;

    @GetMapping
    public ResponseEntity<List<Film>> findAll(){
        return ResponseEntity.ok(filmService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Film> getById(@PathVariable("id") Long id){
        return ResponseEntity.ok(filmService.getById(id));
    }

    @PostMapping
    public ResponseEntity create(@RequestBody Film film){
        return ResponseEntity.ok(filmService.create(film));
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable ("id") Long id){
        filmService.delete(id);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody Film film){
        return ResponseEntity.ok(filmService.update(film));
    }

}
