package az.cinemaplus.cinemaplus.controller;

import az.cinemaplus.cinemaplus.entity.Film;
import az.cinemaplus.cinemaplus.entity.Time;
import az.cinemaplus.cinemaplus.service.TimeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/times")
public class TimeController {
    private final TimeService timeService;

    @GetMapping
    public ResponseEntity<List<Time>> findAll(){
        return ResponseEntity.ok(timeService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Time> getById(@PathVariable("id") Long id){
        return ResponseEntity.ok(timeService.getById(id));
    }

    @PostMapping
    public ResponseEntity create(@RequestBody Time time){
        return ResponseEntity.ok(timeService.create(time));
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable ("id") Long id){
        timeService.delete(id);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody Time time){
        return ResponseEntity.ok(timeService.update(time));
    }


}
