package az.cinemaplus.cinemaplus.controller;

import az.cinemaplus.cinemaplus.entity.Film;
import az.cinemaplus.cinemaplus.entity.Hall;
import az.cinemaplus.cinemaplus.service.HallService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/halls")
public class HallController {
    private final HallService hallService;

    @GetMapping
    public ResponseEntity<List<Hall>> findAll(){
        return ResponseEntity.ok(hallService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Hall> getById(@PathVariable("id") Long id){
        return ResponseEntity.ok(hallService.getById(id));
    }

    @PostMapping
    public ResponseEntity create(@RequestBody Hall hall){
        return ResponseEntity.ok(hallService.create(hall));
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable ("id") Long id){
        hallService.delete(id);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody Hall hall){
        return ResponseEntity.ok(hallService.update(hall));
    }

}
