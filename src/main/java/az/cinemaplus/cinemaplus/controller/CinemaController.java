package az.cinemaplus.cinemaplus.controller;

import az.cinemaplus.cinemaplus.entity.Cinema;
import az.cinemaplus.cinemaplus.entity.Film;
import az.cinemaplus.cinemaplus.service.CinemaService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/cinemas")
public class CinemaController {
    private final CinemaService cinemaService;

    @GetMapping
    public ResponseEntity<List<Cinema>> findAll(){
        return ResponseEntity.ok(cinemaService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Cinema> getById(@PathVariable("id") Long id){
        return ResponseEntity.ok(cinemaService.getById(id));
    }

    @PostMapping
    public ResponseEntity create(@RequestBody Cinema cinema){
        return ResponseEntity.ok(cinemaService.create(cinema));
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable ("id") Long id){
        cinemaService.delete(id);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody Cinema cinema){
        return ResponseEntity.ok(cinemaService.update(cinema));
    }

}
