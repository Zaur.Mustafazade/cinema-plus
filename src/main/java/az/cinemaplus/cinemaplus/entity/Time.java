package az.cinemaplus.cinemaplus.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "times")
@Data
@Entity
public class Time {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "time")
    private String time;

    @ManyToOne
    @JsonIgnore
    private Film film;

    @OneToMany
    @JoinColumn(name = "time_id")
    private List<Cinema> cinemaList = new ArrayList<>();
}
