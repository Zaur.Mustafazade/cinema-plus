package az.cinemaplus.cinemaplus.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "films")
@Data
@Entity
public class Film {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "filmName")
    private String filmName;

    @OneToMany
    @JoinColumn(name = "film_id")
    private List<Time> timeList = new ArrayList<>();
}
