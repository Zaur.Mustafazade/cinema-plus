package az.cinemaplus.cinemaplus.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Table(name = "halls")
@Data
@Entity
public class Hall {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "hallName")
    private String hallName;

    @ManyToOne
    @JsonIgnore
    private Cinema cinema;
}
