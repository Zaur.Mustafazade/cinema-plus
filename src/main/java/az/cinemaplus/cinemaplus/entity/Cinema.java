package az.cinemaplus.cinemaplus.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "cinemas")
@Data
@Entity
public class Cinema {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "cinema")
    private String cinema;

    @ManyToOne
    @JoinColumn(name = "cinema_id")
    @JsonIgnore
    private Time time;

    @OneToMany
    @JoinColumn(name = "cinema_id")
    private List<Hall> hallList = new ArrayList<>();


}
