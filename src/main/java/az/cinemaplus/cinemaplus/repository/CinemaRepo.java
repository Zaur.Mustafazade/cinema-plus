package az.cinemaplus.cinemaplus.repository;

import az.cinemaplus.cinemaplus.entity.Cinema;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CinemaRepo extends JpaRepository<Cinema,Long> {
}
