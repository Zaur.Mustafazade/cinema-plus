package az.cinemaplus.cinemaplus.repository;

import az.cinemaplus.cinemaplus.entity.Time;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TimeRepo extends JpaRepository<Time,Long> {
}
