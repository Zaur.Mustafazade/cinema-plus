package az.cinemaplus.cinemaplus.repository;

import az.cinemaplus.cinemaplus.entity.Film;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmRepo extends JpaRepository<Film, Long> {
}
