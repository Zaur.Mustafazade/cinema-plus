package az.cinemaplus.cinemaplus.repository;

import az.cinemaplus.cinemaplus.entity.Hall;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HallRepo extends JpaRepository<Hall,Long> {
}
