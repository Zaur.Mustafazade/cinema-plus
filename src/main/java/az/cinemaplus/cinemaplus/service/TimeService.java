package az.cinemaplus.cinemaplus.service;

import az.cinemaplus.cinemaplus.entity.Film;
import az.cinemaplus.cinemaplus.entity.Time;
import az.cinemaplus.cinemaplus.repository.TimeRepo;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TimeService {
    private final TimeRepo timeRepo;

    public List<Time> getAll(){
        return timeRepo.findAll();
    }

    public Time getById(Long id){
        return timeRepo.findById(id).get();
    }

    public Time create(Time time){
        return timeRepo.save(time);
    }

    public void delete(Long id){
        Time timeForDelete = timeRepo.findById(id).get();
        timeRepo.delete(timeForDelete);
    }

    public Time update(Time time){
        Time timeForUpdate = timeRepo.getById(time.getId());
        timeForUpdate.setTime(time.getTime());
        timeForUpdate.setFilm(time.getFilm());
        timeForUpdate.setCinemaList(time.getCinemaList());

        return timeRepo.save(timeForUpdate);
    }
}
