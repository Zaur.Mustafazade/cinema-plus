package az.cinemaplus.cinemaplus.service;

import az.cinemaplus.cinemaplus.entity.Film;
import az.cinemaplus.cinemaplus.entity.Hall;
import az.cinemaplus.cinemaplus.repository.HallRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class HallService {
    private final HallRepo hallRepo;

    public List<Hall> getAll(){
        return hallRepo.findAll();
    }

    public Hall getById(Long id){
        return hallRepo.findById(id).get();
    }

    public Hall create(Hall hall){
        return hallRepo.save(hall);
    }

    public void delete(Long id){
        Hall hallForDelete = hallRepo.findById(id).get();
        hallRepo.delete(hallForDelete);
    }

    public Hall update(Hall hall){
        Hall hallForUpdate = hallRepo.getById(hall.getId());
        hallForUpdate.setHallName(hall.getHallName());

        return hallRepo.save(hallForUpdate);
    }
}
