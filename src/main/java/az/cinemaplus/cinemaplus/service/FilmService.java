package az.cinemaplus.cinemaplus.service;

import az.cinemaplus.cinemaplus.entity.Film;
import az.cinemaplus.cinemaplus.repository.FilmRepo;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FilmService {

    private final FilmRepo filmRepo;

    public List<Film> getAll(){
        return filmRepo.findAll();
    }

    public Film getById(Long id){
        return filmRepo.findById(id).get();
    }

    public Film create(Film film){
        return filmRepo.save(film);
    }

    public void delete(Long id){
        Film filmForDelete = filmRepo.findById(id).get();
        filmRepo.delete(filmForDelete);
    }

    public Film update(Film film){
        Film filmForUpdate = filmRepo.getById(film.getId());
        filmForUpdate.setFilmName(film.getFilmName());
        filmForUpdate.setTimeList(film.getTimeList());

        return filmRepo.save(filmForUpdate);
    }
}
