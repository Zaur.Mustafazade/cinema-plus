package az.cinemaplus.cinemaplus.service;

import az.cinemaplus.cinemaplus.entity.Cinema;
import az.cinemaplus.cinemaplus.entity.Film;
import az.cinemaplus.cinemaplus.repository.CinemaRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CinemaService {
    private final CinemaRepo cinemaRepo;

    public List<Cinema> getAll(){
        return cinemaRepo.findAll();
    }

    public Cinema getById(Long id){
        return cinemaRepo.findById(id).get();
    }

    public Cinema create(Cinema cinema){
        return cinemaRepo.save(cinema);
    }

    public void delete(Long id){
        Cinema cinemaForDelete = cinemaRepo.findById(id).get();
        cinemaRepo.delete(cinemaForDelete);
    }

    public Cinema update(Cinema cinema){
        Cinema cinemaForUpdate = cinemaRepo.getById(cinema.getId());
        cinemaForUpdate.setCinema(cinema.getCinema());
        cinemaForUpdate.setHallList(cinema.getHallList());

        return cinemaRepo.save(cinemaForUpdate);
    }
}
